import React from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import {
  bookAppointment,
  moreInfo
} from '../../../modules/card'

import Card from '../card'

import './card-section.css'

const CardSection = props => (
  <section className="card-section">
  {
    props.cards !== undefined ?
    (
      props.cards.map((card, index) => (
        <Card
          key={index}
          imageUrl={card.imageUrl}
          name={card.name}
          address={card.address}
          bookAppointment={props.bookAppointment}
          moreInfo={props.moreInfo}
        />
      ))
    ) :
    undefined
  }
  </section>
)

const mapStateToProps = state => ({
  cards: state.card.cards
})

const mapDispatchToProps = dispatch => bindActionCreators({
  bookAppointment,
  moreInfo
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CardSection)
