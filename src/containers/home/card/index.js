import React from 'react'

import './card.css'

const Card = props => (
  <div className="card">
    <img src={props.imageUrl} alt="card_image" className="card__image" />
    <div className="card__description">
      <span className="card__description--name">{props.name}</span>
      <span className="card__description--address">{props.address}</span>
    </div>
    <div className="card__action">
      <button className="card__action--book-appointment" onClick={props.bookAppointment}>book appointment</button>
      <button className="card__action--more-info" onClick={props.moreInfo}>more info</button>
    </div>
  </div>
)

export default Card
