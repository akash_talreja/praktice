import React from 'react'

import './footer.css'

const Footer = props => (
  <div className="footer">
    <span>{props.text}</span>
  </div>
)

export default Footer
