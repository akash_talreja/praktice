import React from 'react'
import classnames from 'classnames'

import './header.css'

const Header = ({ isVisible }) => (
  <div className={classnames('header', { hide: !isVisible })}>
    <span>Praktice.ai Assignment</span>
  </div>
)

export default Header
