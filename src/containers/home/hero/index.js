import React from 'react'

import './hero.css'

const Hero = () => (
  <div className="hero">
    <span>Praktice.ai Assignment</span>
  </div>
)

export default Hero
