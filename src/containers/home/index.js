import React, { Component } from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { findDOMNode } from 'react-dom'

import {
  toggleHeaderVisibility
} from '../../modules/home'

import Hero from './hero'
import Header from './header'
import Footer from './footer'
import CardSection from './card-section'

import fire from '../../fire'

import './home.css'

class Home extends Component {
  constructor(props) {
    super(props)

    this.onHandleScroll = this.onHandleScroll.bind(this)
  }

  componentDidMount() {
    findDOMNode(this.refs.home)
    .addEventListener('scroll', this.onHandleScroll, false)

    const messaging = fire.messaging()
    messaging.requestPermission()
    .then(() => {
      console.log("Notification permission granted")
      messaging.getToken()
      .then(currentToken => {
        if (currentToken) {
          console.log(currentToken)
        } else {
      // Show permission request.
          console.log('No Instance ID token available. Request permission to generate one.');
          // Show permission UI.
        }
      })
      .catch(err => {
        console.log('An error occurred while retrieving token. ', err);
      });
    })
    .catch( err => {
      console.log('unable to get permission to notify.')
    })
  }

  componentWillUnmount() {
    findDOMNode(this.refs.home)
    .removeEventListener('scroll', this.onHandleScroll, false)
  }

  onHandleScroll(event) {
    if (event.currentTarget.scrollTop === 232) {
      this.props.toggleHeaderVisibility()
    }
  }

  render() {
    return (
      <div className="home" ref="home">
        <Hero />
        <Header
          isVisible={this.props.headerVisibility}
        />
        <CardSection />
        <Footer
          text="Ready to Rock"
        />
      </div>
    )
  }
}

const mapStateToProps = state => ({
  headerVisibility: state.home.headerVisibility
})

const mapDispatchToProps = dispatch => bindActionCreators({
  toggleHeaderVisibility,
  changePage: () => push('/about-us')
}, dispatch)

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home)
