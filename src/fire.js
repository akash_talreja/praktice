import firebase from 'firebase'

const config = {
  apiKey: "AIzaSyBCgYiT94LgpUMRCIxbEDwZE_yyRnkDZnk",
  authDomain: "praktice-ai.firebaseapp.com",
  databaseURL: "https://praktice-ai.firebaseio.com",
  projectId: "praktice-ai",
  storageBucket: "praktice-ai.appspot.com",
  messagingSenderId: "212353070718"
}

const fire = firebase.initializeApp(config)

export default fire
