importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/3.5.2/firebase-messaging.js');

const config = {
  apiKey: "AIzaSyBCgYiT94LgpUMRCIxbEDwZE_yyRnkDZnk",
  authDomain: "praktice-ai.firebaseapp.com",
  databaseURL: "https://praktice-ai.firebaseio.com",
  projectId: "praktice-ai",
  storageBucket: "praktice-ai.appspot.com",
  messagingSenderId: "212353070718"
}

firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(({data} = {}) => {
  const title = data.title || 'Title';
  const opts = Object.assign({
    body: data.body || 'Body'
  }, data);

  return self.registration.showNotification(title, opts);
});
