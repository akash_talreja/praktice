export const MORE_INFO = 'card/MORE_INFO'
export const BOOK_APPOINTMENT = 'card/BOOK_APPOINTMENT'

const initialState = {
  cards: [{
    imageUrl: 'https://lh6.googleusercontent.com/-iwsr3yM64sU/Vu_Td4V1p_I/AAAAAAAAACQ/RvJamzMto9MF0T8cC-_4IUY8t7H5Al1igCLIB/s1600-w400/',
    name: 'Ridgetop Dental International M G Road \u2b50\u2b50\u2b50\u2b50\u2b50',
    address: '154, Trinity Church Complex Trinity Circle M.G. Road Opposite 1MG Mall, Be jashkjh dakjhksjadh sakjh sakjh sdakjhasd asdkjhas kjhda askjh asdjhsakjd as sdakjhjk sadhsdakjh asdhksadkj',
  },
  {
    imageUrl: 'https://tranquil-tor-47635.herokuapp.com/static/images/dental-clinic.jpg',
    name: 'Cosmetic Dental Center smetic Dental Cesmetic Dental Cesmetic Dental Cesmetic Dental Cesmetic Dental Cesmetic Dental Cesmetic Dental Cesmetic Dental Cesmetic Dental Ce',
    address: '22, Jaya Mansion, Marriappa Road, Near-Lido Mall, Ulsoor, Bengalura Road, Near-Lido Mall, Ulsoor, Bengalura Road, Near-Lido Mall, Ulsoor, Bengalur',
  },
  {
    imageUrl: 'https://tranquil-tor-47635.herokuapp.com/static/images/dental-clinic.jpg',
    name: 'Deepak Dental Diagnostic Centre',
    address: '9, Trinity Church Complex, Old Madras Road, Near-Ulsoor Police Station, Ul...',
  }
]}

export default (state = initialState, action) => {
  switch (action.type) {
    case MORE_INFO:
      return state
    case BOOK_APPOINTMENT:
      return state
    default:
      return state
  }
}

export const moreInfo = () => {
  return dispatch => {
    dispatch({
      type: MORE_INFO
    })
  }
}

export const bookAppointment = () => {
  return dispatch => {
    dispatch({
      type: BOOK_APPOINTMENT
    })
  }
}
