export const TOGGLE_HEADER_VISIBILITY = 'home/TOGGLE_HEADER_VISIBILITY'

const initialState = {
  headerVisibility: false
}

export default (state=initialState, action) => {
  switch (action.type) {
    case TOGGLE_HEADER_VISIBILITY:
      return { ...state, headerVisibility: !state.headerVisibility }
    default:
      return state
  }
}

export const toggleHeaderVisibility = () => {
  return dispatch => dispatch({
    type: TOGGLE_HEADER_VISIBILITY
  })
}
