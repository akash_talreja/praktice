import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import card from './card'
import home from './home'

export default combineReducers({
  routing: routerReducer,
  card,
  home
})
